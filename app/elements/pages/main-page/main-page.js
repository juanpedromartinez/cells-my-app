{
  class MainPage extends Polymer.mixinBehaviors([CellsBehaviors.i18nBehavior] , Polymer.Element) {
    constructor() {
      super();
    }

    static get is() {
      return 'main-page';
    }

    onGetName(ev) {
      this.dispatchEvent(new CustomEvent('get-name', {
        bubbles: true,
        composed: true
      }));
    }

    printName(data) {
      console.log(data);
      let button = this.shadowRoot.querySelector('button');
      
      button.style.background = 'red';
      button.textContent = data.name;

      console.log('Parent:', this.parentElement);

    }
  }
  customElements.define(MainPage.is, MainPage);
}

