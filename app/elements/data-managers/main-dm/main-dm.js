{
  class MainDM extends Polymer.Element {
    constructor() {
      super();
    }

    static get is() {
      return 'main-dm';
    }

    static get properties() {
      return {
        name: {
          type: String,
          value: ''
        }
      };
    }

    getName() {
      const { name } = this;
      const getLoaded = 'get-name-loaded';

      this.dispatchEvent(new CustomEvent(getLoaded, {
        bubbles: true,
        composed: true,
        detail: {
          name
        }
      }));
    }

    setName(data) {
      let {page} = data;
      this.set('name', page);
    }

    onPageEnter(evt) {
      this.setName(evt);
    }
  }

  customElements.define(MainDM.is, MainDM);
}