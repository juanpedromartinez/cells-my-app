module.exports = () => {
  return {
    name: {
      get: "get_name_channel",
      set: "set_name_channel",
      getLoaded: 'get_name_loaded_channel'
    }
  }
};