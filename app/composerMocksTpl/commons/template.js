module.exports = () => {
  return {
    tag: 'cells-template-paper-drawer-panel',
    properties: {
      disableEdgeSwipe: true,
      hasHeader: false,
      hasFooter: false
    }
  }
}